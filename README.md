#My first App
##Screenshot
![Screenshot](https://github.com/nirooj56/tutorial-electron/blob/master/screenshot.png)

<p align="center">This is a tutorial demo app made using ELECTRON Framework. [Electron](http://electron.atom.io) (formerly known as Atom Shell) is an open-source framework developed by GitHub. It allows for the development of desktop GUI applications with web technologies like **HTML, CSS and JavaScript**. <p>
##Get the Code
You can get this code by downloading or cloning this repo from the GIthub itself for from the [releases section](http://github.com/nirooj56/tutorial-electron/releases).

Everything in the scripts has been well-commented for the better understanding. If you encounter any problem of didn't understand anything, feel free to contact me or read the [documentations](http://electron.atom.io/docs/) on the wiki. For further knowledge about different aspects of building Desktop GUI app with electron dive into their [documentations](http://electron.atom.io/docs/).

##Run the App
To run this app you will need nodejs installed on you system. Get the stable version from their [website](http://www.nodejs.org) as required by your OS. Next thing you will need is electron compiled version so you can just drag and drop the folder and get the app running. Get it for [Windows (64-bit)](https://github.com/electron/electron/releases/download/v1.4.10/electron-v1.4.10-win32-x64.zip), [Windows(32-bit)](https://github.com/electron/electron/releases/download/v1.4.10/electron-v1.4.10-win32-ia32.zip), [Linux(64-bit)](https://github.com/electron/electron/releases/download/v1.4.10/electron-v1.4.10-linux-x64.zip), [Linux (32-bit)](https://github.com/electron/electron/releases/download/v1.4.10/electron-v1.4.10-linux-ia32.zip) and [MacOS](https://github.com/electron/electron/releases/download/v1.4.10/electron-v1.4.10-darwin-x64.zip) whichever suits you best.
