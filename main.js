const {app, BrowserWindow, Menu} = require('electron')
const {ipcMain} = require('electron')
const electron = require('electron')
var path = require('path')
let win

function createWindow(){
  //creates a new browser window in below mentioned details
  win = new BrowserWindow({
    title: 'My First App', //Gives title to the wpp window
    width: 600,				//Sets the width of the app window
    height: 550,				//Sets the height of the app window
    icon: path.join(__dirname, '/logo.png'),	//Sets App icon for the app
    backgroundColor: 'grey', //Sets background color while the app starts
  })
  
  // if you want to load the webpage in the app just remove '//' from the line below.
  //win.loadURL('https://www.google.com')
  
  // if you want to load the index.html in the app, just remove '//' from the line below.
	win.loadURL(`file://${__dirname}/src/html/index.html`)


}
//when the app is ready electron will create a window in details mentioned above
app.on('ready', createWindow)

// this function closes the main app once clicked on "close this window"
ipcMain.on('close-main-window', function () {
	app.quit();
});

//this creates the new browser window when clicked on "Open a New Window"
app.on('ready', () => {
	ipcMain.on('create-new-window', () => {
		let newWindow = new BrowserWindow({
			title: 'About',
			width: 400,
			height: 459,
			icon: path.join(__dirname, '/logo.png')
		});
		newWindow.setMenu(null);
		newWindow.loadURL('file://' + __dirname + '/src/html/newwindow.html');
		newWindow.show();
	});
});


// Following creates a Menu Items in your app.
// If you dont want any menu, just delete the below code.
const template = [
	{
		role: 'help',
		submenu: [{
				label: 'Learn More about the app',
				click: function () {
					electron.shell.openExternal('https://github.com/nirooj56/tutorial-electron/')
				},
  }, {
  				label: 'Learn More about the creator',
				click: function () {
					electron.shell.openExternal('https://github.com/nirooj56/')
				},
	}, {
				label: 'About',
				click: function (item, focusedWindow) {
					if (focusedWindow) {
						const options = {
							type: 'info',
							title: 'About the App',
							buttons: ['Close'],
							message: 'My First Desktop App.\nVersion: 1.0.0 \nCrafted by Cycotech.'
						}
						electron.dialog.showMessageBox(focusedWindow, options, function () {})
					}
				}
  }
    ]
  }
]

const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu)

